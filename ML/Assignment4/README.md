Q1. To implement k-means use Synthetic dataset.

    Synthetic dataset can be generated using make_blobs() function as follows:

from sklearn.datasets import make_blobs
features, true_labels = make_blobs(n_samples=200,centers=3,cluster_std=2.75,random_state=42)

    Use elbow method to find appropriate number of clusters

Q.2 Agglomerative Clustering: Use Mall_customers.csv (attached)
