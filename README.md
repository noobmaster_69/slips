### Installing `micro` editor

1. Download executable: 

    ```bash
    curl https://getmic.ro | bash
    ```

    Now you'll have `micro` binary in your current working directory. 

2. Move to one of the directory mentioned in `$PATH` :

    _Accustomed setup to our labs_

    ```bash
    mkdir -p ~/perl/bin
    mv micro ~/perl/bin/
    ```

    Now, `micro` should be accessible from any location from console.

3. (Optional) Install plugins to add extra functionality to `micro`

    ```bash
    micro -plugin install quoter
    ```

    For listing all plugins available from repositories:

    ```bash
    micro -plugin available
    ```

    For listing locally installed plugins:

    ```bash
    micro -plugin list
    ```

References:

- [Dive Into Design Patterns - Link 1](https://anonfiles.com/H7C4x0Q5yf/alexander-shvets-dive-into-design-patterns-2019_pdf)
- [Dive Into Design Patterns - Link 2](https://wormhole.app/v80ZJ#jGXpvykPDAG3CY5b1HDsfQ)