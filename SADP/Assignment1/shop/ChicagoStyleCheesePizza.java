public class ChicagoStyleCheesePizza extends Pizza {
	public ChicagoStyleCheesePizza() {
		name = "Chicago Style Cheese Pizza";
		dough = "Extra Thick Crust Dough with Cheese filled";
		sauce = "Plum Tomato Sauce and Cheese sauce";

		toppings.add("Shredded Mozzarella Cheese");
		toppings.add("Cripsy Corn");
	}
}