public class NYStyleVeggiePizza extends Pizza {

	public NYStyleVeggiePizza() {
		name = "NY Style Sauce Veggie Pizza";
		dough = "Thin Crust Dough";
		sauce = "Plum Tomato Sauce";

		toppings.add("Grated Reggiano Cheese");
		toppings.add("Black Olives");
		toppings.add("Spinach");
		toppings.add("Eggplant");
	}
}