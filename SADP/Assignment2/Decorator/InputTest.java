import java.io.*;

public class InputTest {
	public static void main(String[] args) throws IOException {
		if(args.length < 1) throw new IllegalArgumentException("Please provide arguments.");
		InputStream in = null;
		try {
			int c;
			in = new LowerCaseInputStream(new BufferedInputStream(new FileInputStream(args[0])));
			while((c = in.read()) >= 0) {
				System.out.print((char)c);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) { in.close(); }
		}

		try {
			int c;
			in = new LowerCaseInputStream(System.in);
			while((c = in.read()) >= 0){
				System.out.println((char) c);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
