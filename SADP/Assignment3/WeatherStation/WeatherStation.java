import java.util.*;

public class WeatherStation {

	static void setRandomMeasurements(WeatherData weatherData){
		weatherData.setMeasurements(
			(float) Math.random() * (120 - 10 + 1) + 10, 
			(float) Math.random() * (150 - 20 + 1) + 20, 
			(float) Math.random() * (80 - 2 + 1) + 2
		);
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		WeatherData weatherData = new WeatherData();

		CurrentConditionsDisplay currentDisplay = new CurrentConditionsDisplay(weatherData);
		StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
		ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

		while(true){
			System.out.print("Press 'r' to generate new data, 'q' to quit: ");
			switch(sc.nextLine()){
				case "r":
					setRandomMeasurements(weatherData);			
					break;
				case "q":
					System.exit(0);
					break; 
				default:
					System.out.println("Invalid key press event!");					
			}
			System.out.println();
		}

	}
}
