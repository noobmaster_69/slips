public class MenuTestDrive {
	public static void main(String args[]) {
		Waitress waitress = new Waitress(new BreakFastMenu(),  new DinerMenu(), new LunchMenu());
		waitress.printMenu();
	}
}
