const form = document.getElementById('login-form');

const isEmailValid = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

const isLengthZero = (str) => str.length === 0;

form.addEventListener('submit', (e) => {
	e.preventDefault();
	const email = document.getElementById('email').value;
	const pass = document.getElementById('pass').value;

	if(!isEmailValid(email)) alert('Invalid email: E-mail should follow proper email syntax.');
	if(isLengthZero(pass)) alert('Invalid password: Password field cannot be empty.');
	if(isEmailValid(email) && !isLengthZero(pass)){
		msg.innerHTML = "Logged in successfully.";
		form.style.display = 'none';
	}
	else{
		msg.innerHTML = "Login failed. Try again in 5 secs!";		
		form.style.display = 'none';

		setTimeout(() => {
		   window.location.reload();
		   form.style.display = 'block';
		}, 5000);
	}
});
