const form = document.getElementById('register-form');
const isBetween = (number, min, max) => number >= min && number <= max;  
const isAlpha = (str) => /^[a-zA-Z]*$/.test(str);
const isZero = (number) => number == 0 || number == null;
const diffDate = (d1, d2) => {
	const date1 = new Date(d1);
	const date2 = new Date(d2);
	return date2.getTime() - date1.getTime();
}

form.addEventListener('submit', (e) => {
	e.preventDefault();
	const fname = document.getElementById('fname').value;
	const lname = document.getElementById('lname').value;
	const age = document.getElementById('age').value;
	const dob = document.getElementById('dob').value;
	const joindate = document.getElementById('joindate').value;
	const sal = document.getElementById('salary').value;
	const msg = document.getElementById('msg');

	if(!isAlpha(fname)) alert('Invalid first name: Name should only contain alphabets.');
	if(!isAlpha(lname)) alert('Invalid last name: Name should only contain alphabets.')
	if(!isBetween(age, 18, 50)) alert('Invalid age: Age should be between 18 and 50.');
	if(!(diffDate(dob, joindate) > 0)) alert('Invalid joining date: Joining date cannot be earlier from DOB.');
	if(isZero(sal)) alert('Invalid salary: Salary cannot be zero.');

	console.log('Date diff' + diffDate(dob, joindate));
	if(isAlpha(fname) && isAlpha(lname) && isBetween(age, 18, 50) && diffDate(dob, joindate) > 0 && !isZero(sal)){
		msg.innerHTML = "Form submitted successfully.";
		form.style.display = 'none';
	}
	else{
		msg.innerHTML = "Try Again in 5 secs! Form not submitted.";		
		form.style.display = 'none';

		setTimeout(() => {
		   window.location.reload();
		   form.style.display = 'block';
		}, 5000);
	}
});
