const http = require('http')
const current = require('./modules')
const port = 8080;

console.log(`Server URL: http://localhost:${port}`);
http.createServer((req, res) => {
	res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(`<h1>Current date and time: ${current()}</h1>`);
    res.end();
}).listen(port);