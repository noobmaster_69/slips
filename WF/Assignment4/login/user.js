// dependencies
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
// connect to database
mongoose.connect('mongodb://localhost/DS', {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const User = mongoose.Schema({
    username: String,
    password: String
});
// Export Model
User.plugin(passportLocalMongoose);

module.exports = mongoose.model('userData', User, 'userData');
