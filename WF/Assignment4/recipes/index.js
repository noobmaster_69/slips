const express = require('express');
const fs = require('fs');
const app = express();

const server = {
	port: 8000,
	hostname: 'localhost'
};

const recipes = JSON.parse(fs.readFileSync('recipes.json'));

app.use('/images', express.static('public'))

app.set('view engine', 'ejs');

app.get("/", (req, res) => {
	res.redirect('/home');
});

app.get("/home", (req, res) => {
	res.render('home', { server: server, recipes: recipes } );	
});

for(const recipe of recipes){
	app.get(`/recipes/${(recipe.name).toLowerCase().replaceAll(" ", "_")}`, (req, res) => {
		res.render('recipe', { recipe: recipe });
	});
}

app.listen(server.port, server.hostname, () => {
	console.log(`Server listening on ${server.port}: http://localhost:${server.port}`);
});
