# Practical Slips

## Notes

- For **WF** questions, please install modules again, irrespective to what `package.json` says. 
- Incomplete Slip 29, 30 (due to duplicates)
- The "Hacker News" clone website, depends on requirements and framework to use, hence a solution is not provided.

## References:

Poll DJango:
https://medium.com/analytics-vidhya/building-a-simple-poll-system-in-django-part-i-a0bfb4fc3699

Login system using Django:
https://learndjango.com/tutorials/django-login-and-logout-tutorial
