const form = document.getElementById('register-form');
const isBetween = (number, min, max) => number >= min && number <= max;  
const isAlpha = (str) => /^[a-zA-Z]*$/.test(str);

form.addEventListener('submit', (e) => {
	e.preventDefault();
	const fname = document.getElementById('fname').value;
	const lname = document.getElementById('lname').value;
	const age = document.getElementById('age').value;
	const msg = document.getElementById('msg');
	
	if(!isAlpha(fname)) alert('Invalid first name: Name should only contain alphabets.');
	if(!isAlpha(lname)) alert('Invalid last name: Name should only contain alphabets.')
	if(!isBetween(age, 18, 50)) alert('Invalid age: Age should be between 18 and 50.');

	if(isAlpha(fname) && isAlpha(lname) && isBetween(age, 18, 50)){
		msg.innerHTML = "Form submitted successfully.";
	}
	else{
		msg.innerHTML = "Try Again! Form not submitted.";		
	}
});
