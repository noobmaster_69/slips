const mongoose = require('mongoose');

const url = 'mongodb://192.168.100.203/DS';

mongoose.connect(url);

const myModel = mongoose.model('customers', mongoose.Schema({ name: String, addr: String, number: Number}));

myModel.find({}, (err, data) => {
    if(err) console.log(err);
    console.log(data);                                              
});
myModel.deleteOne({name: 'Shreyas'})
    .then(() => console.log('Data removed successfully.'))
    .catch((err) => console.log(err));
