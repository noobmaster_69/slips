const fs = require('fs');
const http = require('http');

// Read the file from the filesystem
const fileData = fs.readFileSync('index.html', 'utf-8');

// Create an HTTP server
const server = http.createServer((req, res) => {
  res.writeHead(200, {'Content-Type': 'text/html'});

  // Serve the file
  res.end(fileData);
});

server.listen(3000);
