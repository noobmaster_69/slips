const EventEmitter = require('events');
var eventEmitter = new EventEmitter();
const readline = require('readline');

eventEmitter.on('hello', () => console.log('Hiya!'));
eventEmitter.on('bye', () => console.log('Seeya sucker...'));

readline.emitKeypressEvents(process.stdin);
if (process.stdin.isTTY) process.stdin.setRawMode(true);
process.stdin.on('keypress', (chunk, key) => {
	if (key){
		if (key.name === 'h') eventEmitter.emit('hello');
		if (key.name === 'b') eventEmitter.emit('bye');
		if (key.name === 'q') process.exit();
	}
});