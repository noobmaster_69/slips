from django.urls import path
from . import views

urlpatterns = [
    path('', views.temp),
    path('test', views.test),
]
