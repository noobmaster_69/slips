from django.http import HttpResponse
from django.template import loader

def temp(request):
  template = loader.get_template('myfirst.html')
  return HttpResponse(template.render())

def test(request):
  return HttpResponse('<h1>Test page</h1>')
