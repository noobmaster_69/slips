import java.util.*;

public class RemoteControlTest {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		SimpleRemoteControl remote = new SimpleRemoteControl();

		Light light = new Light();
		GarageDoor garageDoor = new GarageDoor();

		LightOnCommand lightOn = new LightOnCommand(light);
		LightOffCommand lightOff = new LightOffCommand(light);
		
		GarageDoorOpenCommand garageOpen = new GarageDoorOpenCommand(garageDoor);
		GarageDoorCloseCommand garageClose = new GarageDoorCloseCommand(garageDoor);

 		boolean flag;
 		while(true){
 			flag = false;
 			System.out.print("Press 'loff' to light off, 'lon' to light on, 'gup' to garage door open, 'gdwn' to garagee door close: ");
 			switch(sc.nextLine()){
 				case "loff":
 					remote.setCommand(lightOff);
 					break;
 				case "lon":
 					remote.setCommand(lightOn);
 					break;
 				case "gup":
 					remote.setCommand(garageOpen);
 					break;
 				case "gdwn":
 					remote.setCommand(garageClose);
 					break;
            case "q":
               System.exit(0);
               break;
				default:
					System.out.println("Invalid command!");								
					flag = true;
 			}
			if(!flag) remote.buttonWasPressed();
 		}
    }
	
}
