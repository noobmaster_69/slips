import java.util.ArrayList;
import java.util.Iterator;

public class BreakFastMenu implements Menu {
	ArrayList<MenuItem> menuItems;
 
	public BreakFastMenu() {
		menuItems = new ArrayList<MenuItem>();
    
		addItem("Fruit Plate", 
			"Exotic fruits", 
			true,
			100);
 
		addItem("Semolina Crêpe", 
			"Thin, crispy crêpe with thick sour sauce", 
			true,
			180);
 
		addItem("British Ginger Tea",
			"Tea with added flavour of Ginger",
			true,
			3.49);
 
		addItem("Waffles",
			"Waffles with your choice of blueberries or strawberries",
			true,
			300);
	}

	public void addItem(String name, String description,
	                    boolean vegetarian, double price)
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.add(menuItem);
	}
 
	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}
  
	public Iterator<MenuItem> createIterator() {
		return menuItems.iterator();
	}
  
	// other menu methods here
}
