import java.util.*;

public class LunchMenu implements Menu {
	HashMap<String, MenuItem> menuItems = new HashMap<String, MenuItem>();
  
	public LunchMenu() {
		addItem("White Sauce Pasta",
			"Spaghetti, white sauce pasta with cream and cheese",
			true, 250);
		addItem("Tandoori Cheese Burst Pizza",
			"Pizza crust with filled cheese, and al-carte toppings, cooked in Tandoor",
			true, 210);
		addItem("Chicken Peri-Peri Pizza",
			"Spicy pizza with marinated chicken nuggets, and al-carte toppings",
			true, 310);
	}
 
	public void addItem(String name, String description, 
	                     boolean vegetarian, double price) 
	{
		MenuItem menuItem = new MenuItem(name, description, vegetarian, price);
		menuItems.put(name, menuItem);
	}
 
	public Map<String, MenuItem> getItems() {
		return menuItems;
	}
  
	public Iterator<MenuItem> createIterator() {
		return menuItems.values().iterator();
	}
}
