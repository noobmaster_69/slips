import java.util.Iterator;
  
public class Waitress {
	Menu breakFastMenu;
	Menu dinerMenu;
	Menu lunchMenu;
 
	public Waitress(Menu breakfastMenu, Menu dinerMenu, Menu lunchMenu) {
		this.breakFastMenu = breakfastMenu;
		this.dinerMenu = dinerMenu;
		this.lunchMenu = lunchMenu;
	}
 
	public void printMenu() {
		Iterator<MenuItem> breakFastIterator = breakFastMenu.createIterator();
		Iterator<MenuItem> dinerIterator = dinerMenu.createIterator();
		Iterator<MenuItem> lunchIterator = lunchMenu.createIterator();

		System.out.println("MENU\n----\nBREAKFAST");
		printMenu(breakFastIterator);
		System.out.println("\nLUNCH");
		printMenu(lunchIterator);
		System.out.println("\nDINNER");
		printMenu(dinerIterator);
	}
 
	private void printMenu(Iterator<MenuItem> iterator) {
		while (iterator.hasNext()) {
			MenuItem menuItem = iterator.next();
			System.out.print(menuItem.getName() + ", ");
			System.out.print(menuItem.getPrice() + " -- ");
			System.out.println(menuItem.getDescription());
		}
	}

}

