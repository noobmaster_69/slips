const mongoose = require('mongoose');

const url = 'mongodb://192.168.100:203/DS'

mongoose.connect(url);

const studentModel = mongoose.model('students', mongoose.Schema({
	rollno: Number,
	name: String,
	div: String
}));

const records = [
	{rollno: 12, name: 'Divs', div:'A'},
	{rollno: 13, name: 'K10', div:'A'},
	{rollno: 14, name: 'Shreyas', div:'B'}
];


studentModel
	.insertMany(records)
	.then(() => console.log('Data inserted'))
	.catch((err) => console.log(err));
