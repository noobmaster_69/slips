public class ChicagoStyleClamPizza extends Pizza {
	public ChicagoStyleClamPizza() {
		name = "Chicago Style Clam Pizza";
		dough = "Extra Thick Crust Dough";
		sauce = "Sautéed Broccoli Rabe";

		toppings.add("Shredded Mozzarella Cheese");
		toppings.add("Clams");
		toppings.add("Shallot");
	}

	public void cut() {
		System.out.println("Cutting the pizza into circular sector slices.");
	}
}