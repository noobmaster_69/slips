import java.util.*;

public class AdapterMain{

	public static void main(String[] args) {
		Vector<String> v = new Vector<String>(Arrays.asList(args));
		Iterator<?> iterator = new EnumerationIterator(v.elements());
		int count = 0;
		while (iterator.hasNext()) {
			System.out.println("Element " + (++count) + ": " + iterator.next());			
		}
	}
}
