const fs = require('fs');
const rl = require('readline').createInterface(process.stdin, process.stdout);

rl.question('First file name: ', (f1) => {
	fs.readFile(f1, 'utf8', (err, data) => {
		if(err) throw err;
		rl.question('Second file name: ', (f2) => {
			fs.appendFile(f2, data, 'utf8', (err) => {
				if(err) throw err;
				console.log('Data appended successfully.');
			});
		});
	});
});