const fs = require('fs');
const http = require('http');

const FILENAME = 'lorem.txt';
const PORT = 8000;
const HOSTNAME = '127.0.0.1';

http.createServer((req, res) => {
	fs.readFile(FILENAME, 'utf8', (err, data) => {
		if(err) throw err;
		res.end(data);
	});
}).listen(PORT, HOSTNAME);

console.log(`Server listening on http://${HOSTNAME}:${PORT}`)
