import java.util.*;

public class RemoteLoader {
 
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		RemoteControlWithUndo remoteControl = new RemoteControlWithUndo();
 
		CeilingFan ceilingFan = new CeilingFan("Living Room");

		Command ceilingFanLow = new CeilingFanLowCommand(ceilingFan);   
		Command ceilingFanMedium = new CeilingFanMediumCommand(ceilingFan);
		Command ceilingFanHigh = new CeilingFanHighCommand(ceilingFan);
		Command ceilingFanOff = new CeilingFanOffCommand(ceilingFan); 
		
		remoteControl.setCommand(0, ceilingFanLow, ceilingFanOff);
		remoteControl.setCommand(1, ceilingFanMedium, ceilingFanOff);
		remoteControl.setCommand(2, ceilingFanHigh, ceilingFanOff);

		while(true){
			System.out.println("\nCeiling Fan Control");
			System.out.println("1. OFF");
			System.out.println("2. LOW");
			System.out.println("3. MED");
			System.out.println("4. HIGH");
			System.out.println("5. UNDO");
			System.out.println("6. STATUS");
			System.out.println("7. EXIT");
			System.out.print("\nEnter command: ");
			switch(sc.nextInt()){
				case 1:
					remoteControl.offButtonWasPushed(0);				
					break;
				case 2:
					remoteControl.onButtonWasPushed(0);
					break;
				case 3:
					remoteControl.onButtonWasPushed(1);
					break;
				case 4:
					remoteControl.onButtonWasPushed(2);
					break;
				case 5:
					remoteControl.undoButtonWasPushed();
					break;
				case 6:
					System.out.println(remoteControl);
					break;
				case 7:
					System.out.println("Bye...");
					System.exit(0);
					break;
				default:
					System.out.println("Invalid command! Try again...");
			}	
		}
	}
}
